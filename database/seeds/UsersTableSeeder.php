<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'      => 'SuperAdministrator',
            'email'     => 'superadmin@gmail.com',
            'password'  => bcrypt('1234'),
            'role'  => 'superadmin',
        ]);
        DB::table('users')->insert([
            'name'      => 'Admin',
            'email'     => 'admin@gmail.com',
            'password'  => bcrypt('1234'),
            'role'  => 'admin',
        ]);
    }
}
